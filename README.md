# Streamlit 3D Plot Example

This repository contains a simple streamlit application with an interactive 3D plot.

## Instructions

Build docker image

```bash
docker build -t streamlit-3d:latest .
```

Run docker container

```bash
docker run -p 8501:8501 streamlit-3d:latest
```

Access the printed link.