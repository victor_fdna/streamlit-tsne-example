import streamlit as st
import numpy as np
import plotly.express as px

df = px.data.iris()
fig = px.scatter_3d(df, x='sepal_length', y='sepal_width', z='petal_width',
              color='species')

st.plotly_chart(fig, use_container_width=True)